<?php

namespace SearchBundle\Service;

use SearchBundle\Index\Index;

/**
 * Class IndexProvider
 */
class IndexProvider
{
    /**
     * @var array
     */
    private $indexes;

    /**
     * @var array
     */
    private $config;

    /**
     * IndexProvider constructor.
     * @param array $config
     */
    public function __construct($config)
    {
        $this->config = $config;
        $this->indexes = [];
    }

    /**
     * Add index by name
     * @param string $name
     */
    public function addIndex($name)
    {
        if ($this->validate($name)) {
            $this->indexes[$name] = (new Index())->setName($name)
                ->setAttributes($this->config['indexes'][$name]['attributes'])
                ->setFields(
                    [
                        'main' => $this->config['indexes'][$name]['fields'],
                        'multilang' => $this->config['indexes'][$name]['multilang_fields'],
                    ]
                )
                ->setLanguages($this->config['indexes'][$name]['languages'])
                ->setEntity($this->config['indexes'][$name]['entity']);
        }
    }

    /**
     * Remove index by name
     * @param string $name
     */
    public function removeIndex($name)
    {
        unset($this->indexes[$name]);
    }

    /**
     * Get index by name
     * @param string $name
     *
     * @return Index
     */
    public function getIndex($name)
    {
        $this->validate($name);

        return $this->indexes[$name];
    }

    /**
     * Add all indexes from config
     */
    public function addConfigIndexes()
    {
        foreach ($this->config['indexes'] as $k => $v) {
            $this->addIndex($k);
        }
    }

    /**
     * @return array get all indexes
     */
    public function getIndexes()
    {
        return $this->indexes;
    }

    /**
     * @param string $entity
     *
     * @return Index
     *
     * @throws \Exception
     */
    public function getIndexByEntity($entity)
    {
        foreach ($this->indexes as $index) {
            if ($index->getEntity() === $entity) {
                return $index;
            }
        }

        throw new \Exception('Wrong entity name');
    }

    /**
     * Validate index by name
     * @param string $name
     *
     * @throws \Exception
     *
     * @return bool
     */
    private function validate($name)
    {
        if (!isset($this->config['indexes'][$name])) {
            throw new \Exception('Wrong index');
        }

        return true;
    }
}