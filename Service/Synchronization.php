<?php

namespace SearchBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use SearchBundle\Index\Index;
use SearchBundle\Index\IndexDataIterator;
use SearchBundle\PersistConnector\PersistConnectorInterface;
use Doctrine\Common\Util\Inflector as Inflector;

/**
 * Class Synchronization
 */
class Synchronization
{
    const BATCH_SIZE = 10;

    /**
     * @var IndexProvider
     */
    private $indexProvider;

    /**
     * @var PersistConnectorInterface
     */
    private $persistConnector;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * Synchronization constructor.
     * @param IndexProvider             $indexProvider
     * @param PersistConnectorInterface $persistConnector
     * @param EntityManagerInterface    $em
     */
    public function __construct(IndexProvider $indexProvider, PersistConnectorInterface $persistConnector, EntityManagerInterface $em)
    {
        $this->indexProvider = $indexProvider;
        $this->indexProvider->addConfigIndexes();
        $this->persistConnector = $persistConnector;
        $this->em = $em;
    }

    /**
     * @param object $object
     */
    public function update($object)
    {
        $index = $this->indexProvider->getIndexByEntity(get_class($object));

        if ($this->persistConnector->select('*')->from($index)->where(['id' => $object->getId()])->execute()) {
            $this->persistConnector->update()
                ->into($index)
                ->set($this->getFields($object, $index))
                ->where(['id' => $object->getId()])
                ->limit(1)
                ->execute();
        } else {
            $this->insert($object);
        }
    }

    /**
     * @param object $object
     */
    public function insert($object)
    {
        $index = $this->indexProvider->getIndexByEntity(get_class($object));
        $this->persistConnector->insert()->into($index)->set($this->getFields($object, $index))->execute();
    }

    /**
     * @param object $object
     */
    public function delete($object)
    {
        $index = $this->indexProvider->getIndexByEntity(get_class($object));
        $this->persistConnector->delete()->from($index)->where(['id' => $object->getId()])->execute();
    }

    /**
     * @param Index         $index
     * @param \Closure|null $output
     * @param integer       $limit
     * @param integer       $offset
     */
    public function insertIndex(Index $index, \Closure $output = null, $limit = 0, $offset = 0)
    {
        $queryBuilder = $this->em->createQueryBuilder();
        $count = $queryBuilder->select('count(e.id)')
            ->from($index->getEntity(), 'e')
            ->getQuery()
            ->getSingleScalarResult();

        $steps = ceil($count / self::BATCH_SIZE);
        $count  = (($count > ($limit + $offset)) and $limit) ? $limit + $offset : $count;

        for ($currentOffset = $offset; $currentOffset < $count; $currentOffset += self::BATCH_SIZE) {
            $queryBuilder = $this->em->createQueryBuilder();
            $queryBuilder = $queryBuilder->select('e')
                ->from($index->getEntity(), 'e')
                ->setFirstResult($currentOffset)
                ->setMaxResults(($limit < self::BATCH_SIZE and $limit) ? $limit : self::BATCH_SIZE);
            $objects = $queryBuilder->getQuery()->getResult();

            foreach ($objects as $object) {
                $this->insert($object);
            }

            if ($output) {
                $step = floor($currentOffset / self::BATCH_SIZE + 1);
                $percentComplete = number_format((100 * $step / $steps), 1);
                $output(sprintf('Step: %s (%s%%)', $step, $percentComplete));
            }
        }
    }

    /**
     * @param Index $index
     */
    public function truncateIndex(Index $index)
    {
        $this->persistConnector->truncate($index->getName());
    }

    /**
     * @param object $object
     * @param Index  $index
     *
     * @return array
     */
    private function getFields($object, Index $index)
    {
        $set = [];
        $set['id'] = $object->getId();

        foreach ($index->getFields() as $field) {
            if ('entity' === $field) {
                $set[$field] = get_class($object);
            } else {
                $set[$field] = $this->normalizeField($object->{Inflector::camelize('get'.$field)}());
            }
        }

        foreach ($index->getFields('multilang') as $field) {
            $set[$field] = [];
            foreach ($index->getLanguages() as $lang) {
                $set[$field][] = $object->translate($lang, false)->{Inflector::camelize('get'.$field)}();
            }
            $set[$field] = implode(' ', $set[$field]);
        }

        return $set;
    }

    /**
     * @param mixed $value
     *
     * @return string
     */
    private function normalizeField($value)
    {
        if ($value instanceof \DateTime) {
            $value = $value->format('U');
        } elseif (is_object($value)) {
            $value = (string) $value->getId();
        } elseif ((!is_scalar($value) && !is_null($value)) || is_int($value) || is_bool($value)) {
            $value = (string) $value;
        }

        return $value;
    }

}