<?php

namespace SearchBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use SearchBundle\DependencyInjection\Compiler\SearchCompilerPass;

class SearchBundle extends Bundle
{

}
