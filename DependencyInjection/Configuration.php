<?php

namespace SearchBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 */
class Configuration implements ConfigurationInterface
{
    /**
     * Generates the configuration tree builder.
     *
     * @return \Symfony\Component\Config\Definition\Builder\TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();

        $rootNode = $treeBuilder->root('search');

        $rootNode
            ->children()
                ->arrayNode('clients')
                    ->requiresAtLeastOneElement()
                    ->prototype('array')
                        ->children()
                            ->scalarNode('log')->end()
                            ->scalarNode('query_log')->end()
                            ->scalarNode('pid_file')->end()
                            ->scalarNode('mysql_host')->end()
                            ->scalarNode('mysql_port')->end()
                            ->scalarNode('thread_stack')->end()
                        ->end()
                    ->end()
                ->end()
                    ->arrayNode('indexes')
                    ->requiresAtLeastOneElement()
                     ->prototype('array')
                        ->children()
                            ->scalarNode('entity')->end()
                            ->arrayNode('fields')
                                ->requiresAtLeastOneElement()->prototype('scalar')->end()
                            ->end()
                            ->arrayNode('multilang_fields')
                                ->requiresAtLeastOneElement()->prototype('scalar')->end()
                            ->end()
                            ->arrayNode('languages')
                                ->requiresAtLeastOneElement()->prototype('scalar')->end()
                            ->end()
                            ->arrayNode('attributes')
                                ->requiresAtLeastOneElement()->prototype('scalar')->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
