<?php

namespace SearchBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Output\Output;

/**
 * Populate the search index
 */
class UpdateCommand extends ContainerAwareCommand
{

    /**
     * configure
     */
    protected function configure()
    {
        $this->setName('search:sphinx:update')->setDescription('Output update sphinx')
            ->addOption('index', null, InputOption::VALUE_OPTIONAL, 'The index to update')
            ->addOption('no-delete', null, InputOption::VALUE_NONE, 'No truncating index')
            ->addOption('limit', null, InputOption::VALUE_OPTIONAL, 'Limit')
            ->addOption('offset', null, InputOption::VALUE_OPTIONAL, 'Offset');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $index = $input->getOption('index');
        $limit = (int) $input->getOption('limit');
        $offset = (int) $input->getOption('offset');
        $delete = $input->getOption('no-delete') ? 0 : 1;

        $indexManager = $this->getContainer()->get('search.index_provider');
        $synchronization = $this->getContainer()->get('search.synchronization');
        $indexManager->addConfigIndexes();

        foreach (($index ? [$indexManager->getIndex($index)] : $indexManager->getIndexes()) as $currentIndex) {
            if ($delete) {
                $output->writeln(sprintf('<info>DELETE index</info> <comment>%s</comment>', $currentIndex->getName()));
                $synchronization->truncateIndex($currentIndex);
            }

            $outputClosure = function ($message) use ($output, $currentIndex) {
                $output->writeln(sprintf('<info>INSERT </info> %s: %s', $currentIndex->getName(), $message));
            };
            $synchronization->insertIndex($currentIndex, $outputClosure, $limit, $offset);
        }
    }
}

