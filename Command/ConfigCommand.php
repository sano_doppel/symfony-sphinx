<?php

namespace SearchBundle\Command;

use SearchBundle\Builder\ConfigBuilder;
use SearchBundle\Builder\SphinxConfig;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Populate the search index
 */
class ConfigCommand extends ContainerAwareCommand
{

    /**
     * configure
     */
    protected function configure()
    {
        $this->setName('search:sphinx:config')->setDescription('Output sphinx config file');
    }


    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $configBuilder = new ConfigBuilder();
        $configBuilder->setConfig(new SphinxConfig());
        $configBuilder->setPath($this->getContainer()->getParameter('kernel.root_dir').'/config/');
        $configBuilder->setParameters($this->getContainer()->getParameter('search_config'));
        $configBuilder->build();

        return null;
    }
}
