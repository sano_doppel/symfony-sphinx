<?php

namespace SearchBundle\Builder;

/**
 * Class ConfigBuilder
 */
class ConfigBuilder
{
    /**
     * @var ConfigInterface
     */
    private $config;

    /**
     * @var
     */
    private $path;

    /**
     * @var
     */
    private $parameters;

    /**
     * @param ConfigInterface $config
     */
    public function setConfig(ConfigInterface $config)
    {
        $this->config = $config;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @param array $parameters
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * build config
     */
    public function build()
    {
        $this->config->build($this->path, $this->parameters);
    }
}