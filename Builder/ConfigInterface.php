<?php

namespace SearchBundle\Builder;

/**
 * Interface ConfigInterface
 */
interface ConfigInterface
{
    /**
     * build function
     * @param string $path
     * @param array  $parameters
     */
    public function build($path, $parameters);

}