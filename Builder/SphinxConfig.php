<?php

namespace SearchBundle\Builder;

/**
 * Class SphinxConfig
 */
class SphinxConfig implements ConfigInterface
{
    /**
     * Build sphinx config
     * @param string $path
     * @param array  $parameters
     *
     * @return array
     */
    public function build($path, $parameters)
    {
        try {
            foreach ($parameters['clients'] as $clientKey => $client) {
                $data = '';
                foreach ($parameters['indexes'] as $key => $index) {
                    $data .= '# rt index: '.$key.PHP_EOL;
                    $data .= 'index '.$key.PHP_EOL;
                    $data .= '{';
                    foreach ($index['attributes'] as $k => $v) {
                        $data .= "\t".$v.PHP_EOL;
                    }
                    $data .= '}'.PHP_EOL;
                }

                $data .= '# searchd config:'.PHP_EOL;
                $data .= 'searchd {'.PHP_EOL;
                $data .= "\t".'binlog_path = '.PHP_EOL;
                $data .= "\t".'log = '.$client['log'].PHP_EOL;
                $data .= "\t".'query_log = '.$client['query_log'].PHP_EOL;
                $data .= "\t".'pid_file = '.$client['pid_file'].PHP_EOL;
                $data .= "\t".'thread_stack = '.$client['thread_stack'].PHP_EOL;
                $data .= "\t".'listen = '.$client['mysql_port'].':mysql41'.PHP_EOL;
                $data .= '}';
                $file = fopen($path.'sphinx.'.$clientKey.'.cfg', 'w+');
                fwrite($file, $data);
                fclose($file);
            }
        } catch (\Exception $e) {
            return ['status' => 'error', 'message' => $e->getMessage()];
        }

        return ['status' => 'success'];
    }
}