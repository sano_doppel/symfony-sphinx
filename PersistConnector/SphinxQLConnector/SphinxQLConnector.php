<?php

namespace SearchBundle\PersistConnector\SphinxQLConnector;

use Foolz\SphinxQL\Drivers\SimpleConnection;
use Foolz\SphinxQL\SphinxQL;
use SearchBundle\PersistConnector\CRUD\Delete;
use SearchBundle\PersistConnector\CRUD\Insert;
use SearchBundle\PersistConnector\CRUD\Select;
use SearchBundle\PersistConnector\CRUD\Update;
use SearchBundle\PersistConnector\PersistConnectorInterface;

/**
 * Interface SphinxQLConnector
 */
class SphinxQLConnector implements PersistConnectorInterface
{
    /**
     * @var $connection
     */
    private $connection;

    /**
     * @var Select|Insert|Update|Delete
     */
    private $persister;

    /**
     * @param array $parameters
     *
     * @return SimpleConnection
     */
    public function setConnection($parameters)
    {
        $this->connection = new SimpleConnection();
        $this->connection->setParams(array(
            'host' => $parameters['host'],
            'port' => $parameters['port'],
        ));

        return $this->connection;
    }

    /**
     * @param string $query
     *
     * @return \Foolz\SphinxQL\Drivers\ResultSetInterface
     */
    public function query($query)
    {
        return SphinxQL::create($this->connection)->query($query)->execute();
    }

    /**
     * @param string $index
     *
     * @return void
     */
    public function truncate($index)
    {
        $this->query(sprintf('TRUNCATE RTINDEX `%s`', $index));
    }

    /**
     * @return Insert
     */
    public function insert()
    {
        $this->reset();
        $this->persister = new Insert(SphinxQL::create($this->connection)->insert());

        return $this->persister;
    }

    /**
     * @return Update
     */
    public function update()
    {
        $this->reset();
        $this->persister = new Update(SphinxQL::create($this->connection)->replace());

        return $this->persister;
    }

    /**
     * @return Delete
     */
    public function delete()
    {
        $this->reset();
        $this->persister = new Delete(SphinxQL::create($this->connection)->delete());

        return $this->persister;
    }

    /**
     * @param string|array $fields
     *
     * @return Select
     */
    public function select($fields = '*')
    {
        $this->reset();
        $this->persister = new Select(
            SphinxQL::create($this->connection)->select($fields ? (is_array($fields) ? implode(',', $fields) : $fields) : '*')
        );

        return $this->persister;
    }

    /**
     * @param string $key
     *
     * @return string
     */
    public function getMeta($key)
    {
        $meta = SphinxQL::create($this->connection)->query("SHOW META LIKE '$key'")->execute();
        if ($meta) {
            list(, $value) = array_values($meta[0]);

            return $value;
        }

        return false;
    }

    /**
     * create objects
     */
    private function reset()
    {
        $this->persister = null;
    }
}