<?php

namespace SearchBundle\PersistConnector\SphinxQLConnector;

use Foolz\SphinxQL\SphinxQL;

/**
 * Class QueryProcessor
 */
class QueryProcessor
{
    /**
     * @var SphinxQL
     */
    protected $query;

    /**
     * Select constructor.
     * @param SphinxQL $query
     */
    public function __construct($query)
    {
        $this->query = $query;
    }

    /**
     * @return SphinxQL
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * Match text
     * @param array|string $text
     *
     * @return SphinxQL
     */
    public function match($text)
    {
        if (is_array($text)) {
            foreach ($text as $k => $v) {
                $this->query = $this->query->match($v['column'], $v['value']);
            }
        } elseif (is_string($text)) {
            $this->query = $this->query->match('*', $text);
        }

        return $this->query;
    }

    /**
     * Set limit
     * @param int $limit
     *
     * @return SphinxQL
     */
    public function limit($limit)
    {
        if ($limit) {
            $this->query = $this->query->limit((int) $limit);
        }

        return $this->query;
    }

    /**
     * Set offset
     * @param int $offset
     *
     * @return SphinxQL
     */
    public function offset($offset)
    {
        $this->query = $this->query->offset((int) $offset);

        return $this->query;
    }

    /**
     * Set order by
     * @param array $order
     *
     * @return SphinxQL
     */
    public function order($order)
    {
        foreach ($order as $k => $v) {
            $this->query = $this->query->orderBy($k, $v);
        }

        return $this->query;
    }

    /**
     * Where condition
     * @param array $condition
     *
     * @return SphinxQL
     */
    public function where($condition)
    {
        foreach ($condition as $k => $v) {
            if (is_array($v) and sizeof($v) === 3) {
                $this->query = $this->query->where($v[0], $v[1], $v[2]);
            } else {
                $this->query = $this->query->where($k, $v);
            }
        }

        return $this->query;
    }

    /**
     * Set data
     * @param array $data
     *
     * @return mixed
     */
    public function set($data)
    {
        $this->query = $this->query->set($data);
    }

    /**
     * Get data from string
     * @param string $index
     *
     * @return SphinxQL
     */
    public function from($index)
    {
        $this->query = $this->query->from($index);

        return $this->query;
    }

    /**
     * Set to index
     * @param string $index
     *
     * @return mixed
     */
    public function into($index)
    {
        return $this->query->into($index);
    }

    /**
     * Execute a query
     *
     * @return array
     */
    public function execute()
    {
        return $this->query->execute();
    }
}