<?php

namespace SearchBundle\PersistConnector;

/**
 * Class PersistConnectorFactory
 */
class PersistConnectorFactory
{
    /**
     * @param string $class
     *
     * @return PersistConnectorInterface
     */
    public static function initial($class)
    {
        return class_exists($class) ? new $class() : null;
    }
}