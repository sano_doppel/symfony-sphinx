<?php

namespace SearchBundle\PersistConnector\CRUD;

use SearchBundle\Index\Index;

/**
 * Interface DeleteInterface
 */
interface DeleteInterface
{
    /**
     * Get data from index
     * @param Index $index
     *
     * @return $this
     */
    public function from(Index $index);

    /**
     * Where condition
     * @param array $condition
     *
     * @return $this
     */
    public function where($condition);

    /**
     * Set limit
     * @param int $limit
     *
     * @return $this
     */
    public function limit($limit);
}
