<?php

namespace SearchBundle\PersistConnector\CRUD;

use SearchBundle\Index\Index;

/**
 * Interface UpdateInterface
 */
interface UpdateInterface
{
    /**
     * Set to index
     * @param Index $index
     *
     * @return $this
     */
    public function into(Index $index);

    /**
     * Set data
     * @param array $data
     *
     * @return $this
     */
    public function set($data);

    /**
     * Where condition
     * @param array $condition
     *
     * @return $this
     */
    public function where($condition);

    /**
     * Set limit
     * @param int $limit
     *
     * @return $this
     */
    public function limit($limit);
}
