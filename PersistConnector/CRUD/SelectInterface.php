<?php

namespace SearchBundle\PersistConnector\CRUD;

use SearchBundle\Index\Index;

/**
 * Interface SelectInterface
 */
interface SelectInterface
{
    /**
     * Match text
     * @param array|string $text
     *
     * @return $this
     */
    public function match($text);

    /**
     * Get data from index
     * @param Index|array $index
     *
     * @return $this
     */
    public function from($index);

    /**
     * Where condition
     * @param array $condition
     *
     * @return $this
     */
    public function where($condition);

    /**
     * Set limit
     * @param int $limit
     *
     * @return $this
     */
    public function limit($limit);

    /**
     * Set offset
     * @param int $offset
     *
     * @return $this
     */
    public function offset($offset);

    /**
     * Set order by
     * @param array $order
     *
     * @return $this
     */
    public function order($order);
}
