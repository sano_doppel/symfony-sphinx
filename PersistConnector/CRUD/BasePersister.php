<?php

namespace SearchBundle\PersistConnector\CRUD;

use SearchBundle\PersistConnector\SphinxQLConnector\QueryProcessor;

/**
 * Class BasePersister
 */
class BasePersister
{
    /**
     * @var QueryProcessor
     */
    protected $processor;

    /**
     * Select constructor.
     * @param object $query
     */
    public function __construct($query)
    {
        $this->processor = new QueryProcessor($query);
    }

    /**
     * Execute a query
     * @return array
     */
    public function execute()
    {
        return $this->processor->execute();
    }
}