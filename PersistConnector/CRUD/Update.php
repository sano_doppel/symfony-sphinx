<?php

namespace SearchBundle\PersistConnector\CRUD;

use SearchBundle\Index\Index;
use SearchBundle\PersistConnector\SphinxQLConnector\QueryProcessor;

/**
 * Class Update
 */
class Update extends BasePersister implements UpdateInterface
{
    /**
     * Set to index
     * @param Index $index
     *
     * @return $this
     */
    public function into(Index $index)
    {
        $this->processor->into($index->getName());

        return $this;
    }

    /**
     * Set data
     * @param array $data
     *
     * @return $this
     */
    public function set($data)
    {
        $this->processor->set($data);

        return $this;
    }

    /**
     * Where condition
     * @param array $condition
     *
     * @return $this
     */
    public function where($condition)
    {
        $this->processor->where($condition);

        return $this;
    }

    /**
     * Set limit
     * @param int $limit
     *
     * @return $this
     */
    public function limit($limit)
    {
        $this->processor->limit($limit);

        return $this;
    }
}
