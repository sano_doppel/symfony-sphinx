<?php

namespace SearchBundle\PersistConnector\CRUD;

use SearchBundle\Index\Index;

/**
 * Class Delete
 */
class Delete extends BasePersister implements DeleteInterface
{
    /**
     * Get data from index
     * @param Index $index
     *
     * @return $this
     */
    public function from(Index $index)
    {
        $this->processor->from($index->getName());

        return $this;
    }

    /**
     * Where condition
     * @param array $condition
     *
     * @return $this
     */
    public function where($condition)
    {
        $this->processor->where($condition);

        return $this;
    }

    /**
     * Set limit
     * @param int $limit
     *
     * @return $this
     */
    public function limit($limit)
    {
        $this->processor->limit($limit);

        return $this;
    }
}