<?php

namespace SearchBundle\PersistConnector\CRUD;

use SearchBundle\Index\Index;

/**
 * Class Select
 */
class Select extends BasePersister implements SelectInterface
{

    /**
     * Match text
     * @param array|string $text
     *
     * @return $this
     */
    public function match($text)
    {
        $this->processor->match($text);

        return $this;
    }

    /**
     * Get data from index
     * @param Index|array $index
     *
     * @return $this
     */
    public function from($index)
    {
        if (!is_array($index)) {
            $index = $index->getName();
        } else {
            $index = array_map(function ($index) {
                return $index->getName();
            }, $index);
        }

        $this->processor->from($index);

        return $this;
    }

    /**
     * Where condition
     * @param array $condition
     *
     * @return $this
     */
    public function where($condition)
    {
        $this->processor->where($condition);

        return $this;
    }

    /**
     * Set limit
     * @param int $limit
     *
     * @return $this
     */
    public function limit($limit)
    {
        $this->processor->limit($limit);
        $this->processor->offset(0);

        return $this;
    }

    /**
     * Set offset
     * @param int $offset
     *
     * @return $this
     */
    public function offset($offset)
    {
        $this->processor->offset($offset);

        return $this;
    }

    /**
     * Set order by
     * @param array $order
     *
     * @return $this
     */
    public function order($order)
    {
        $this->processor->order($order);

        return $this;
    }
}
