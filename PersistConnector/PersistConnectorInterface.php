<?php

namespace SearchBundle\PersistConnector;

use Foolz\SphinxQL\Drivers\SimpleConnection;
use SearchBundle\PersistConnector\CRUD\DeleteInterface;
use SearchBundle\PersistConnector\CRUD\InsertInterface;
use SearchBundle\PersistConnector\CRUD\PersistInterface;
use SearchBundle\PersistConnector\CRUD\SelectInterface;
use SearchBundle\PersistConnector\CRUD\UpdateInterface;

/**
 * Interface PersistConnectorInterface
 */
interface PersistConnectorInterface
{
    /**
     * @param array $parameters
     *
     * @return SimpleConnection
     */
    public function setConnection($parameters);

    /**
     * @param string $query
     *
     * @return mixed
     */
    public function query($query);

    /**
     * @return InsertInterface
     */
    public function insert();

    /**
     * @return UpdateInterface
     */
    public function update();

    /**
     * @return DeleteInterface
     */
    public function delete();

    /**
     * @param string $fields
     *
     * @return SelectInterface
     */
    public function select($fields);

    /**
     * @param string $index
     *
     * @return void
     */
    public function truncate($index);
}
